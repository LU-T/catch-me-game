
#include <graphics.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>

#define N 15                            //Numero de elementos del arreglo de cubos( NxNxN).
#define A 20                            //Longitud del lado del cubo.
#define P 10                            //Profundidad del cubo( hacia arriba).

#define TAM_MENU 3                      //Cantidad de botones para el menu.

#define PANT COLOR( 255, 255, 0)        //Color del fondo.
#define SHADOW_1 COLOR( 219, 59, 77)    //Sombra proyectada hacia atras.
#define SHADOW_2 COLOR( 235, 164, 42)   //Sombra proyectada a la izquierda.
#define SHADOW_3 COLOR( 163, 79, 26)    //Sombra proyectada hacia abajo.

#define PLAYER COLOR( 60, 0, 100)       //Color del cubo del jugador.
#define ROSA COLOR( 245, 136, 108)      //Color de los cubos que siguen al jugador.
#define OBSTACULO COLOR( 125, 90, 100)  //Color de los obstaculos.
#define FRUTA COLOR( 175, 0, 105)       //Color de los cubos que atrapa el jugador.
#define NARANJA COLOR( 255, 160, 0)     //Color automatico de los cubos.

#define REC 4                           //Cantidad de puntajes almacenados en el archivo de records.

typedef struct
{
   int x1;                		//Posicion en x del centro del boton.
   int y1;                		//Posicion en y del centro del boton.
   int r;                 		//Radio del circulo.
   int cON;               		//Color del boton al presionar o poner encima el cursor.
   int cOFF;              		//Color del boton en estado inactivo.
   int letras;            		//Color de las letras.
   char men[10];          		//Texto del boton.
}BOTON;

typedef struct
{
   int xt;                		//Posicion en x del texto.
   int yt;                		//Posicion en y del texto.
   int nBtn;              		//Numero que ocupa un boton en el menu.
   BOTON btns[TAM_MENU];  		//Arreglo de botones del menu.
}MENU;

typedef struct
{
   int x;                 		//Guarda la coordenada x de un cubo.
   int y;                 		//Guarda la coordenada y de un cubo.
   int edo;               		//Estado de un cubo(visible o no).
   int color;             		//Color de cada cubo.
}CUBO;

typedef struct
{
   char nombre[40];       		//Nombre del jugador.
   int puntos;            		//Puntaje del jugador.
}RECORD;

//Portada del juego.
void portada( char *sTxt);	
void textoPortada( int c, int iFuente, char *sTxt);

//Funciones del menu.
void creaMenu( MENU *m);
void menu( MENU m, CUBO iArr[N][N][N]);
void pintaMenu( MENU m);
void pintaBtn( BOTON btn, int edo);
int checaBtn( MENU m, int x, int y);

//Ayuda.
void lee_Ayuda();

//Funciones de la opcion jugar.
void inicializa( CUBO iArr[N][N][N], int nivel);
void dibuja_fondo( int x, int y);
void sombras( CUBO iArr[N][N][N], int x, int y);
void dibuja_contenedor( CUBO iArr[N][N][N]);
void dibuja_cubo( int x, int y, int color);
void dibuja_lineas( int x, int y);
void marcador( int puntos, int nivel, int tiempo);

//Movimientos del cubo.
void jugar( CUBO iArr[N][N][N], int *puntos);
int mueve_der( CUBO iArr[N][N][N], int *perder);
int mueve_izq( CUBO iArr[N][N][N], int *perder);
int mueve_arriba( CUBO iArr[N][N][N], int *perder);
int mueve_abajo( CUBO iArr[N][N][N], int *perder);
int mueve_adelante( CUBO iArr[N][N][N], int *perder);
int mueve_atras( CUBO iArr[N][N][N], int *perder);

//Movimientos de los cubos rivales.
void mueve_Rival( CUBO iArr[N][N][N], int *perder);
void mueve_rIzq( CUBO iArr[N][N][N], int colCub, int *perder);
void mueve_rDer( CUBO iArr[N][N][N], int colCub, int *perder);
void mueve_rUp( CUBO iArr[N][N][N], int renCub, int *perder);
void mueve_rDown( CUBO iArr[N][N][N], int renCub, int *perder);
void mueve_rAtras( CUBO iArr[N][N][N], int capCub, int *perder);
void mueve_rAdelante( CUBO iArr[N][N][N], int capCub, int *perder);

//Giros del contenedor.
void gira_Der( CUBO iArr[N][N][N]);
void gira_Izq( CUBO iArr[N][N][N]);
void gira_up( CUBO iArr[N][N][N]);
void gira_down( CUBO iArr[N][N][N]);
void cubosAbajo( CUBO iArr[N][N][N], int *perder);

//Datos del jugador.
void records( int puntos);
void nombre_jugador( char cadena[]);
void lee_texto( int xText, int yText, char cadena[]);
void retraso( int milisec);
void burbuja( RECORD a[REC]);

int main()
{ 
   MENU m_menu;
   CUBO iArreglo[N][N][N];
   char *sTxt="CATCH ME.";
   
   initwindow( 1000, 600, sTxt);
   portada( sTxt);
   creaMenu( &m_menu);
   menu( m_menu, iArreglo);
   closegraph();
   return ( 0);
}

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*Portada del juego.*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

void portada( char *sTxt)
{
   int c=0, iFuente=4, nPag=0;
   
   setfillstyle(1, YELLOW);
   bar( 0, 0, getmaxx(), getmaxy());
   textoPortada( c, iFuente, sTxt);
   getch();
   for(c=0; c<=255; c++)
   {
      setactivepage(nPag);
      cleardevice();
      textoPortada( c, iFuente, sTxt);
      setvisualpage(nPag);
      if(nPag==0)
         nPag=1;
      else
         nPag=0;
   }
   cleardevice();
}

void textoPortada( int c, int iFuente, char *sTxt)
{   
   setbkcolor( COLOR( 255, 255, 0));
   setcolor( COLOR(c, c, 0));
   settextstyle( iFuente, 0, 9);
   outtextxy(( getmaxx()-textwidth( sTxt))/2, ( getmaxy()-textheight( sTxt))/2+30, sTxt);
   settextstyle( iFuente, 2, 8);
   outtextxy(( getmaxx()+textwidth( sTxt))/2, ( getmaxy()-textheight( sTxt))/2-10, sTxt);
}

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*Funciones del menu.*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

void creaMenu( MENU *m)
{
   m->btns[0].x1=200;
   m->btns[0].y1=300;
   m->btns[0].r=100;
   m->btns[0].cON=COLOR( 255, 160, 0);
   m->btns[0].cOFF=BLACK;
   m->btns[0].letras=WHITE;
   strcpy( m->btns[0].men, "Jugar");
   
   m->btns[1].x1=500;
   m->btns[1].y1=300;
   m->btns[1].r=100;
   m->btns[1].cON=COLOR( 250, 120, 0);
   m->btns[1].cOFF=BLACK;
   m->btns[1].letras=WHITE;
   strcpy( m->btns[1].men, "Ayuda");
   
   m->btns[2].x1=800;
   m->btns[2].y1=300;
   m->btns[2].r=100;
   m->btns[2].cON=COLOR( 255, 80, 0);
   m->btns[2].cOFF=BLACK;
   m->btns[2].letras=WHITE;
   strcpy( m->btns[2].men, "Salir");
   
   m->nBtn=3;                                 //Numero de botones del menu.
}

void menu( MENU m, CUBO iArr[N][N][N])
{
   int op=-1,marca=-1, puntos=0;
   
   pintaMenu( m);
   while(op!=3)
   {
      delay(1);
      if( ismouseclick( WM_MOUSEMOVE))
      {
         op=checaBtn( m, mousex(), mousey());
         if(op>=0 && op!=marca)
         {
            pintaBtn( m.btns[op], 0);
            marca=op;
         }
         else
            if( op==-1 && marca>=0)
            {
               pintaBtn( m.btns[marca], 1);
               marca=-1;
            }
         clearmouseclick( WM_MOUSEMOVE);
      }
      if( ismouseclick( WM_LBUTTONDOWN))
      {
         if( op>=0)
         {
            pintaBtn( m.btns[op], 0);
            delay( 150);
            pintaBtn( m.btns[op], 1);
            delay( 150);
            cleardevice();
            switch( op)
            {
               case 0:
                  jugar( iArr, &puntos);
                  records( puntos);
		  op=3;
                  break;
               case 1:
                  setfillstyle( 1, YELLOW);
                  bar( 0 ,0, getmaxx(), getmaxy());
                  lee_Ayuda();
                  getch();
                  cleardevice();
                  pintaMenu( m);
                  break;
               case 2:
                  op=3;
                  break;
            }
         }
         clearmouseclick( WM_LBUTTONDOWN);
      }
   }
}

void pintaMenu( MENU m)
{
   int i;
   
   setfillstyle(1, YELLOW);
   bar( 0,0,getmaxx(), getmaxy());
   for( i=0; i<m.nBtn; i++)
      pintaBtn( m.btns[i], 1);
}

void pintaBtn( BOTON btn, int edo)
{
   int xc, yc;
   
   setcolor( BLACK);
   setlinestyle( 0, 1, 1);
   setfillstyle( 1, btn.cOFF);
   fillellipse( btn.x1, btn.y1, btn.r, btn.r);
   settextstyle( 5, 0, 3);
   xc=btn.x1-textwidth( btn.men)/2;
   yc=btn.y1-textheight( btn.men)/2;
   if( edo)
   {
      setcolor( BLACK);
      setbkcolor( btn.cON);
      setfillstyle( 1, btn.cON);
      fillellipse( btn.x1, btn.y1, btn.r, btn.r);
      outtextxy( xc, yc, btn.men);
   }
   else
   {
      setbkcolor( btn.cOFF);
      setfillstyle( 1, btn.cOFF);
      fillellipse( btn.x1, btn.y1, btn.r, btn.r);
      setfillstyle( 1, YELLOW);
      fillellipse( btn.x1-btn.r/2, btn.y1-btn.r/4, btn.r/4, btn.r/4);
      fillellipse( btn.x1+btn.r/2, btn.y1-btn.r/4, btn.r/4, btn.r/4);
      setcolor( YELLOW);
      arc( btn.x1, btn.y1, 200, 340, btn.r/2);
   }
}

int checaBtn( MENU m, int x, int y)
{
   int i,band=-1;
   float d=0;                                         
   
   for( i=0; band<0 && i<m.nBtn; i++)
   {
      d=sqrt(pow(x-m.btns[i].x1, 2)+pow(y-m.btns[i].y1, 2));
      if(d<=(float)m.btns[i].r)
         band=i;
   }
   return( band);
}

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*Ayuda.*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

void lee_Ayuda()
{    
   int y=30;
   FILE *f;
   char c[255];
   
   settextstyle( 4, 0, 3);
   setbkcolor( PANT);
   f=fopen( "Ayuda.txt", "r");
   setcolor( SHADOW_1);
   while( !feof(f))
   {
      fgets( c, 255, f);
      c[ strlen(c)-1]='\0';
      outtextxy( (getmaxx()-textwidth(c))/2, y, c);
      y+=textheight("H")+2;
   }
   fclose( f);
   getch();
}

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*Funciones de la opcion jugar.*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

void inicializa( CUBO iArr[N][N][N], int nivel)
{
   int ren, col, cap;
   
   for(cap=0; cap<N; cap++)
      for( ren=N-1; ren>=0; ren--)
         for( col=0; col<N; col++)
         {
            iArr[ren][col][cap].color=NARANJA;
            iArr[ren][col][cap].edo=0;
	        iArr[ren][col][cap].x=240+col*A-cap*P;
            iArr[ren][col][cap].y=80+ren*A+cap*A/2;
         }
   if(nivel>0 && nivel<4)
   {
      iArr[0][0][0].color=ROSA;
      iArr[0][0][0].edo=1;
      iArr[N-1][N-1][N-1].color=ROSA;
      iArr[N-1][N-1][N-1].edo=1;
      iArr[(N-1-(N-1)%2)/2][(N-1-(N-1)%3)/3][(N-1-(N-1)%3)/3].color=OBSTACULO;
      iArr[(N-1-(N-1)%2)/2][(N-1-(N-1)%3)/3][(N-1-(N-1)%3)/3].edo=1;
      iArr[(N-1-(N-1)%2)/2][(N-1-(N-1)%3)/3][(N-1-(N-1)%3)*2/3].color=OBSTACULO;
      iArr[(N-1-(N-1)%2)/2][(N-1-(N-1)%3)/3][(N-1-(N-1)%3)*2/3].edo=1;
      iArr[(N-1-(N-1)%2)/2][(N-1-(N-1)%3)*2/3][(N-1-(N-1)%3)/3].color=OBSTACULO;
      iArr[(N-1-(N-1)%2)/2][(N-1-(N-1)%3)*2/3][(N-1-(N-1)%3)/3].edo=1;
      iArr[(N-1-(N-1)%2)/2][(N-1-(N-1)%3)*2/3][(N-1-(N-1)%3)*2/3].color=OBSTACULO;
      iArr[(N-1-(N-1)%2)/2][(N-1-(N-1)%3)*2/3][(N-1-(N-1)%3)*2/3].edo=1;
      //Frutas de la capa de atras.      
      iArr[(N-1-(N-1)%4)/4][(N-1-(N-1)%4)/4][(N-1-(N-1)%4)/4].color=FRUTA;
      iArr[(N-1-(N-1)%4)/4][(N-1-(N-1)%4)/4][(N-1-(N-1)%4)/4].edo=1;  
      iArr[(N-1-(N-1)%4)/4][((N-1-(N-1)%4)/4)*3+1][(N-1-(N-1)%4)/4].color=FRUTA;
      iArr[(N-1-(N-1)%4)/4][((N-1-(N-1)%4)/4)*3+1][(N-1-(N-1)%4)/4].edo=1;      
      iArr[(N-1-(N-1)%2)/2][(N-1-(N-1)%2)/2][(N-1-(N-1)%4)/4].color=FRUTA;
      iArr[(N-1-(N-1)%2)/2][(N-1-(N-1)%2)/2][(N-1-(N-1)%4)/4].edo=1;
      iArr[((N-1-(N-1)%4)/4)*3+1][(N-1-(N-1)%4)/4][(N-1-(N-1)%4)/4].color=FRUTA;
      iArr[((N-1-(N-1)%4)/4)*3+1][(N-1-(N-1)%4)/4][(N-1-(N-1)%4)/4].edo=1;  
      iArr[((N-1-(N-1)%4)/4)*3+1][((N-1-(N-1)%4)/4)*3+1][(N-1-(N-1)%4)/4].color=FRUTA;
      iArr[((N-1-(N-1)%4)/4)*3+1][((N-1-(N-1)%4)/4)*3+1][(N-1-(N-1)%4)/4].edo=1; 
      //Frutas de la capa de el centro.           
      iArr[(N-1-(N-1)%4)/4][(N-1-(N-1)%2)/2][(N-1-(N-1)%2)/2].color=FRUTA;
      iArr[(N-1-(N-1)%4)/4][(N-1-(N-1)%2)/2][(N-1-(N-1)%2)/2].edo=rand()%2;
      iArr[((N-1-(N-1)%4)/4)*3+1][(N-1-(N-1)%2)/2][(N-1-(N-1)%2)/2].color=FRUTA;
      iArr[((N-1-(N-1)%4)/4)*3+1][(N-1-(N-1)%2)/2][(N-1-(N-1)%2)/2].edo=rand()%2;
      iArr[(N-1-(N-1)%2)/2][(N-1-(N-1)%4)/4][(N-1-(N-1)%2)/2].color=FRUTA;
      iArr[(N-1-(N-1)%2)/2][(N-1-(N-1)%4)/4][(N-1-(N-1)%2)/2].edo=rand()%2;  
      iArr[(N-1-(N-1)%2)/2][((N-1-(N-1)%4)/4)*3+1][(N-1-(N-1)%2)/2].color=FRUTA;
      iArr[(N-1-(N-1)%2)/2][((N-1-(N-1)%4)/4)*3+1][(N-1-(N-1)%2)/2].edo=rand()%2;
      //Frutas de la capa de adelante.      
      iArr[(N-1-(N-1)%4)/4][(N-1-(N-1)%4)/4][((N-1-(N-1)%4)/4)*3+1].color=FRUTA;
      iArr[(N-1-(N-1)%4)/4][(N-1-(N-1)%4)/4][((N-1-(N-1)%4)/4)*3+1].edo=1;
      iArr[(N-1-(N-1)%4)/4][((N-1-(N-1)%4)/4)*3+1][((N-1-(N-1)%4)/4)*3+1].color=FRUTA;
      iArr[(N-1-(N-1)%4)/4][((N-1-(N-1)%4)/4)*3+1][((N-1-(N-1)%4)/4)*3+1].edo=1;
      iArr[(N-1-(N-1)%2)/2][(N-1-(N-1)%2)/2][((N-1-(N-1)%4)/4)*3+1].color=FRUTA;
      iArr[(N-1-(N-1)%2)/2][(N-1-(N-1)%2)/2][((N-1-(N-1)%4)/4)*3+1].edo=1;
      iArr[((N-1-(N-1)%4)/4)*3+1][(N-1-(N-1)%4)/4][((N-1-(N-1)%4)/4)*3+1].color=FRUTA;
      iArr[((N-1-(N-1)%4)/4)*3+1][(N-1-(N-1)%4)/4][((N-1-(N-1)%4)/4)*3+1].edo=1;
      iArr[((N-1-(N-1)%4)/4)*3+1][((N-1-(N-1)%4)/4)*3+1][((N-1-(N-1)%4)/4)*3+1].color=FRUTA;
      iArr[((N-1-(N-1)%4)/4)*3+1][((N-1-(N-1)%4)/4)*3+1][((N-1-(N-1)%4)/4)*3+1].edo=1;
      if(nivel>1)
      {
         iArr[0][0][N-1].color=ROSA;
         iArr[0][0][N-1].edo=1;
         iArr[N-1][N-1][0].color=ROSA;
         iArr[N-1][N-1][0].edo=1;
         //Frutas de la capa de atras.  
         iArr[(N-1-(N-1)%4)/4][(N-1-(N-1)%2)/2][(N-1-(N-1)%4)/4].color=FRUTA;
         iArr[(N-1-(N-1)%4)/4][(N-1-(N-1)%2)/2][(N-1-(N-1)%4)/4].edo=rand()%2;
         iArr[((N-1-(N-1)%4)/4)*3+1][(N-1-(N-1)%2)/2][(N-1-(N-1)%4)/4].color=FRUTA;
         iArr[((N-1-(N-1)%4)/4)*3+1][(N-1-(N-1)%2)/2][(N-1-(N-1)%4)/4].edo=rand()%2;
         //Frutas de la capa de el centro.  
         iArr[((N-1-(N-1)%4)/4)*3+1][(N-1-(N-1)%4)/4][(N-1-(N-1)%2)/2].color=FRUTA;
         iArr[((N-1-(N-1)%4)/4)*3+1][(N-1-(N-1)%4)/4][(N-1-(N-1)%2)/2].edo=1;
         iArr[((N-1-(N-1)%4)/4)*3+1][((N-1-(N-1)%4)/4)*3+1][(N-1-(N-1)%2)/2].color=FRUTA;
         iArr[((N-1-(N-1)%4)/4)*3+1][((N-1-(N-1)%4)/4)*3+1][(N-1-(N-1)%2)/2].edo=1;
         //Frutas de la capa de adelante. 
         iArr[(N-1-(N-1)%4)/4][(N-1-(N-1)%2)/2][((N-1-(N-1)%4)/4)*3+1].color=FRUTA;
         iArr[(N-1-(N-1)%4)/4][(N-1-(N-1)%2)/2][((N-1-(N-1)%4)/4)*3+1].edo=rand()%2;
         iArr[((N-1-(N-1)%4)/4)*3+1][(N-1-(N-1)%2)/2][((N-1-(N-1)%4)/4)*3+1].color=FRUTA;
         iArr[((N-1-(N-1)%4)/4)*3+1][(N-1-(N-1)%2)/2][((N-1-(N-1)%4)/4)*3+1].edo=rand()%2;
         if(nivel>2)
         {
            iArr[N-1][0][N-1].color=ROSA;
            iArr[N-1][0][N-1].edo=1;
            iArr[0][N-1][0].color=ROSA;
            iArr[0][N-1][0].edo=1;
            //Frutas de la capa de atras. 
            iArr[(N-1-(N-1)%2)/2][(N-1-(N-1)%4)/4][(N-1-(N-1)%4)/4].color=FRUTA;
            iArr[(N-1-(N-1)%2)/2][(N-1-(N-1)%4)/4][(N-1-(N-1)%4)/4].edo=rand()%2;  
            iArr[(N-1-(N-1)%2)/2][((N-1-(N-1)%4)/4)*3+1][(N-1-(N-1)%4)/4].color=FRUTA;
            iArr[(N-1-(N-1)%2)/2][((N-1-(N-1)%4)/4)*3+1][(N-1-(N-1)%4)/4].edo=rand()%2;
            //Frutas de la capa de el centro.  
            iArr[(N-1-(N-1)%4)/4][(N-1-(N-1)%4)/4][(N-1-(N-1)%2)/2].color=FRUTA;
            iArr[(N-1-(N-1)%4)/4][(N-1-(N-1)%4)/4][(N-1-(N-1)%2)/2].edo=1;  
            iArr[(N-1-(N-1)%4)/4][((N-1-(N-1)%4)/4)*3+1][(N-1-(N-1)%2)/2].color=FRUTA;
            iArr[(N-1-(N-1)%4)/4][((N-1-(N-1)%4)/4)*3+1][(N-1-(N-1)%2)/2].edo=1;
            //Frutas de la capa de adelante. 
            iArr[(N-1-(N-1)%2)/2][(N-1-(N-1)%4)/4][((N-1-(N-1)%4)/4)*3+1].color=FRUTA;
            iArr[(N-1-(N-1)%2)/2][(N-1-(N-1)%4)/4][((N-1-(N-1)%4)/4)*3+1].edo=rand()%2;  
            iArr[(N-1-(N-1)%2)/2][((N-1-(N-1)%4)/4)*3+1][((N-1-(N-1)%4)/4)*3+1].color=FRUTA;
            iArr[(N-1-(N-1)%2)/2][((N-1-(N-1)%4)/4)*3+1][((N-1-(N-1)%4)/4)*3+1].edo=rand()%2;
         }
      }
   }
   iArr[(N-N%2)/2][(N-N%2)/2][(N-N%2)/2].color=PLAYER;
   iArr[(N-N%2)/2][(N-N%2)/2][(N-N%2)/2].edo=1;
}

void dibuja_fondo( int x, int y)
{
   int poly[8];
   
   setfillstyle( 1, PANT);
   bar( 0, 0, getmaxx(), getmaxy());
   
   //Cara posterior del contenedor.
   setfillstyle( 1, COLOR(255, 56, 78));
   poly[0]=x+P;
   poly[1]=y-A/2;
   poly[2]=poly[0];
   poly[3]=poly[1]+N*A;
   poly[4]=poly[0]+N*A;
   poly[5]=poly[3];
   poly[6]=poly[4];
   poly[7]=poly[1];
   fillpoly( 4, poly);
   
   //Cara lateral del contenedor.
   setfillstyle( 1, COLOR( 255, 160, 0));
   poly[0]=x-(N-1)*P;
   poly[1]=y+(N-1)*A/2;
   poly[2]=x+P;
   poly[3]=y-A/2;
   poly[4]=poly[2];
   poly[5]=poly[3]+N*A;
   poly[6]=poly[0];
   poly[7]=poly[1]+N*A;
   fillpoly( 4, poly);
   
   //Cara inferior del contenedor.
   setfillstyle( 1, COLOR( 185, 96, 42));
   poly[0]=x+P;
   poly[1]=y-A/2+N*A;
   poly[2]=poly[0]+N*A;
   poly[3]=poly[1];
   poly[4]=x+N*A-(N-1)*P;
   poly[5]=poly[1]+N*A/2;
   poly[6]=poly[4]-N*A;
   poly[7]=poly[5];
   fillpoly( 4, poly);
}

void sombras( CUBO iArr[N][N][N], int x, int y)
{
   int ren, col, cap, poly[8];
   
   for( cap=0; cap<N; cap++)
      for( ren=0; ren<N; ren++)
         for( col=0; col<N; col++)
            if(iArr[ren][col][cap].edo==1)
            {
               setfillstyle( 1, SHADOW_1);
               setcolor(SHADOW_1);
               bar( x+P+A*col, y-A/2+A*ren, x+P+col*A+A, y-A/2+A*ren+A);
               rectangle( x+A*col+P, y-A/2+A*ren, x+P+A*col+A, y-A/2+A*ren+A);
               setfillstyle( 1, SHADOW_2);
               setcolor(SHADOW_2);
               poly[0]=x-cap*P;
               poly[1]=y+ren*A+cap*A/2;
               poly[2]=poly[0]+P;
               poly[3]=poly[1]-A/2;
               poly[4]=poly[2];
               poly[5]=poly[3]+A;
               poly[6]=poly[0];
               poly[7]=poly[1]+A;
               fillpoly( 4, poly);
               setfillstyle( 1, SHADOW_3);
               setcolor(SHADOW_3);
               poly[0]=x+P+col*A-cap*P;
               poly[1]=y-A/2+N*A+cap*A/2;
               poly[2]=poly[0]+A;
               poly[3]=poly[1];
               poly[4]=poly[2]-P;
               poly[5]=poly[3]+A/2;
               poly[6]=poly[0]-P;
               poly[7]=poly[5];
               fillpoly( 4, poly);
            }
}

void dibuja_contenedor( CUBO iArr[N][N][N])
{
   int ren, col, cap;
   
   for( cap=0; cap<N; cap++)
      for( ren=N-1; ren>=0; ren--)
         for( col=0; col<N; col++)
            if(iArr[ren][col][cap].edo)
               dibuja_cubo( iArr[ren][col][cap].x, iArr[ren][col][cap].y, iArr[ren][col][cap].color);
}

void dibuja_cubo( int x, int y, int color)
{
   int poly[8];
     
   setfillstyle( 1, color);
   setcolor( COLOR( 250, 235, 0));
      
   bar ( x, y, x+A, y+A);		             
   rectangle( x, y, x+A, y+A);

   poly[0]=x;         			             
   poly[1]=y;         
   poly[2]=x+A/2;     
   poly[3]=y-P;       
   poly[4]=x+A+A/2;   
   poly[5]=y-P;       
   poly[6]=x+A;
   poly[7]=y;         
   fillpoly( 4, poly);   		           
      
   poly[0]=x+A;       			             
   poly[1]=y;
   poly[2]=x+A+A/2;
   poly[3]=y-P;
   poly[4]=x+A+A/2;
   poly[5]=y+A-P;
   poly[6]=x+A;
   poly[7]=y+A;
   fillpoly( 4, poly);			            
}

void dibuja_lineas( int x, int y)
{
   setcolor( PANT);                                                
   line( x-(N-1)*P, y+(N-1)*A/2, x-(N-1)*P+N*A, y+(N-1)*A/2);          
   line( x-(N-1)*P+N*A, y+(N-1)*A/2,  x-(N-1)*P+N*A, y+(N-1)*A/2+N*A);
   line( x-(N-1)*P+N*A, y+(N-1)*A/2,  x+P+N*A, y-A/2);
}

void marcador( int puntos, int nivel, int tiempo)
{
   char mensaje[40];

   setbkcolor(COLOR( 255, 255, 0));			 
   settextstyle( 4, 0, 5);				 
   setcolor( COLOR( 76, 58, 0));			           
   outtextxy( 950-textwidth("Puntaje:"), 70, "Puntaje:");
   outtextxy( 950-textwidth("Nivel:"), 210, "Nivel:");
   outtextxy( 950-textwidth("Tiempo:"), 350, "Tiempo:");

   setcolor( COLOR( 255, 0, 0));                       
   sprintf( mensaje, " %d", puntos);
   outtextxy( 950-textwidth( mensaje), 120, mensaje);	    
   sprintf( mensaje, " %d", nivel);
   outtextxy( 950-textwidth( mensaje), 260, mensaje);	  
   sprintf( mensaje, " %d", tiempo);
   outtextxy( 950-textwidth( mensaje), 400, mensaje);	  
}

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*Movimientos del cubo.*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

void jugar(CUBO iArr[N][N][N], int *puntos)
{
   int x=240, y=80, nPag=0, iTecla, perder=0, niv=1, vel=1000;
   clock_t t1, t2, ti, t;     		           
   char sTxt[20];
   
   inicializa( iArr, niv);			           
   ti=clock();			     		     
   t1=ti;					
   do{
      do{                        
         setactivepage( nPag);
         cleardevice();          
         dibuja_fondo( x, y);      
         sombras( iArr, x, y);	
         t=(clock()-ti)/1000;
         marcador( *puntos, niv, t);   
         dibuja_contenedor( iArr);		  
         dibuja_lineas( x, y);
         setvisualpage( nPag);
         if(nPag==0)
            nPag=1;
         else
            nPag=0;
         t2=clock();
         if( t2-t1>=vel)
         {
            t1=t2;
            mueve_Rival( iArr, &perder);	 
         }
         t=(clock()-ti)/1000;
         if(t/niv>=30)	
         {
            niv++;
            inicializa( iArr, niv);
            vel-=100;
         }
      }while(!kbhit() && perder==0);
      iTecla=getch();
      if(iTecla==0)
         iTecla=getch();		
      switch( iTecla)
      {					
         case 77: *puntos+=mueve_der( iArr, &perder);    
                 break;
         case 75: *puntos+=mueve_izq( iArr, &perder);     
                 break; 
         case 72: *puntos+=mueve_arriba( iArr, &perder);    
                 break;
         case 80: *puntos+=mueve_abajo( iArr, &perder);  
                 break;
         case 'A': case 'a': *puntos+=mueve_adelante( iArr, &perder);
                 break;
         case 'Z': case 'z': *puntos+=mueve_atras( iArr, &perder); 
                 break;
         case 'D': case 'd':				   
                 gira_Der( iArr);			   
                 cubosAbajo( iArr, &perder);
                 break;
         case 'I': case 'i':
                 gira_Izq( iArr);
                 cubosAbajo( iArr, &perder); 
                 break;
         case 'N': case 'n':
                 gira_up( iArr);	
                 cubosAbajo( iArr, &perder);  
                 break;
         case 'B': case 'b':
                 gira_down( iArr);	
                 cubosAbajo( iArr, &perder);  
                 break;
      } 
   }while(perder!=1 && niv<4);
}

int mueve_der( CUBO iArr[N][N][N], int *perder)
{
   int ren, col, cap, mov=0;
   
   for( cap=N-1; cap>=0; cap--)
      for( ren=0; ren<N; ren++)
         for( col=N-2; col>=0; col--)
            if(iArr[ren][col][cap].color==PLAYER && iArr[ren][col+1][cap].color!=OBSTACULO)
            {
               if(iArr[ren][col+1][cap].color==ROSA && iArr[ren][col+1][cap].edo==1)
                  *perder=1;
               else 
                  if(iArr[ren][col+1][cap].color==FRUTA && iArr[ren][col+1][cap].edo==1)  
                     mov=1000;                        
               iArr[ren][col][cap].color=NARANJA;             
               iArr[ren][col][cap].edo=0;
               iArr[ren][col+1][cap].color=PLAYER;         
               iArr[ren][col+1][cap].edo=1;
               mov+=1;
            }      
   return( mov);    
}

int mueve_izq( CUBO iArr[N][N][N], int *perder)
{
   int ren, col, cap, mov=0;
 
   for( cap=0; cap<N; cap++)
      for( ren=0; ren<N; ren++)
         for( col=1; col<N; col++)
            if(iArr[ren][col][cap].color==PLAYER && iArr[ren][col-1][cap].color!=OBSTACULO)
            {
               if(iArr[ren][col-1][cap].color==ROSA && iArr[ren][col-1][cap].edo==1)
                  *perder=1;                                 
               else                                                
                  if(iArr[ren][col-1][cap].color==FRUTA && iArr[ren][col-1][cap].edo==1)  
                     mov=1000;                                       
               iArr[ren][col][cap].color=NARANJA;                     
               iArr[ren][col][cap].edo=0;
               iArr[ren][col-1][cap].color=PLAYER;                      
               iArr[ren][col-1][cap].edo=1;
               mov+=1;
            }
   return( mov);                                                     
}

int mueve_arriba( CUBO iArr[N][N][N], int *perder)
{
   int ren, col, cap, mov=0;
 
   for( cap=0; cap<N; cap++)
      for( ren=1; ren<N; ren++)
         for( col=0; col<N; col++)
            if(iArr[ren][col][cap].color==PLAYER && iArr[ren-1][col][cap].color!=OBSTACULO) 
            {
               if(iArr[ren-1][col][cap].color==ROSA && iArr[ren-1][col][cap].edo==1)
                  *perder=1;                                   
               else                                                        
                  if(iArr[ren-1][col][cap].color==FRUTA && iArr[ren-1][col][cap].edo==1)  
                     mov=1000;                                  
               iArr[ren][col][cap].color=NARANJA;                
               iArr[ren][col][cap].edo=0;
               iArr[ren-1][col][cap].color=PLAYER;               
               iArr[ren-1][col][cap].edo=1;
               mov+=1;
            }
   return( mov);                         
}

int mueve_abajo( CUBO iArr[N][N][N], int *perder)
{
   int ren, col, cap, mov=0;
   
   for( cap=N-1; cap>=0; cap--)
      for( ren=N-2; ren>=0; ren--)
         for( col=N-1; col>=0; col--)
            if(iArr[ren][col][cap].color==PLAYER && iArr[ren+1][col][cap].color!=OBSTACULO)
            {
               if(iArr[ren+1][col][cap].color==ROSA && iArr[ren+1][col][cap].edo==1)
                  *perder=1;         
               else                                                    
                  if(iArr[ren+1][col][cap].color==FRUTA && iArr[ren+1][col][cap].edo==1) 
                     mov=1000;               
               iArr[ren][col][cap].color=NARANJA;                  
               iArr[ren][col][cap].edo=0;
               iArr[ren+1][col][cap].color=PLAYER;           
               iArr[ren+1][col][cap].edo=1;
               mov+=1; 
            }
   return( mov);                 
}

int mueve_adelante( CUBO iArr[N][N][N], int *perder)
{
   int ren, col, cap, mov=0;
   
   for( cap=N-2; cap>=0; cap--)
      for( ren=N-1; ren>=0; ren--)
         for( col=N-1; col>=0; col--)
            if(iArr[ren][col][cap].color==PLAYER && iArr[ren][col][cap+1].color!=OBSTACULO)
            {
               if(iArr[ren][col][cap+1].color==ROSA && iArr[ren][col][cap+1].edo==1)
                  *perder=1;          
               else                                                   
                  if(iArr[ren][col][cap+1].color==FRUTA && iArr[ren][col][cap+1].edo==1) 
                     mov=1000;                      
               iArr[ren][col][cap].color=NARANJA;       
               iArr[ren][col][cap].edo=0;
               iArr[ren][col][cap+1].color=PLAYER;                           
               iArr[ren][col][cap+1].edo=1;
               mov+=1;
            }
   return( mov);                                                       
}

int mueve_atras( CUBO iArr[N][N][N], int *perder)
{
   int ren, col, cap, mov=0;
 
   for( cap=1; cap<N; cap++)
      for( ren=0; ren<N; ren++)
         for( col=0; col<N; col++)
            if(iArr[ren][col][cap].color==PLAYER && iArr[ren][col][cap-1].color!=OBSTACULO)
            {
               if(iArr[ren][col][cap-1].color==ROSA && iArr[ren][col][cap-1].edo==1)
                  *perder=1;                                                             
               else                                                                     
                  if(iArr[ren][col][cap-1].color==FRUTA && iArr[ren][col][cap-1].edo==1)   
                  mov=1000;                                                          
               iArr[ren][col][cap].color=NARANJA;                        
               iArr[ren][col][cap].edo=0;
               iArr[ren][col][cap-1].color=PLAYER;                             
               iArr[ren][col][cap-1].edo=1;
               mov+=1;
            }  
   return( mov);                                        
}

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*Movimientos de los cubos rivales.*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

void mueve_Rival( CUBO iArr[N][N][N], int *perder)
{
   int ren, col, cap, renCub, colCub, capCub;
   
   for( cap=0; cap<N; cap++)                      
      for( ren=0; ren<N; ren++)
         for( col=0; col<N; col++)
            if(iArr[ren][col][cap].color==PLAYER) 
            {
               mueve_rIzq( iArr, col, perder);    
               mueve_rDer( iArr, col, perder);      
               mueve_rUp( iArr, ren, perder);   
               mueve_rDown( iArr, ren, perder);  
               mueve_rAtras( iArr, cap, perder);   
               mueve_rAdelante( iArr, cap, perder); 
               col=ren=cap=N;
            }
}

void mueve_rIzq( CUBO iArr[N][N][N], int colCub, int *perder)
{
   int ren, col, cap;
   
   for( cap=0; cap<N; cap++)
      for( ren=0; ren<N; ren++)
         for( col=0; col<N; col++)
            if( iArr[ren][col][cap].color==ROSA)         
               if( colCub<col)                             
               {
                  if(iArr[ren][col-1][cap].color==PLAYER)   
                     *perder=1;                  
                  if(iArr[ren][col-1][cap].color==PLAYER || iArr[ren][col-1][cap].color==NARANJA) 
                  {
                     iArr[ren][col][cap].color=NARANJA;                           
                     iArr[ren][col][cap].edo=0;
                     iArr[ren][col-1][cap].color=ROSA;               
                     iArr[ren][col-1][cap].edo=1;
                  }
               }
}

void mueve_rDer( CUBO iArr[N][N][N], int colCub, int *perder)
{
   int ren, col, cap;
   
   for( cap=N-1; cap>=0; cap--)
      for( ren=N-1; ren>=0; ren--)
         for( col=N-1; col>=0; col--)
            if( iArr[ren][col][cap].color==ROSA)   
               if( colCub>col)                 
               {
                  if(iArr[ren][col+1][cap].color==PLAYER)   
                     *perder=1;                            
                  if(iArr[ren][col+1][cap].color==PLAYER || iArr[ren][col+1][cap].color==NARANJA)  
                  {
                     iArr[ren][col][cap].color=NARANJA;                             
                     iArr[ren][col][cap].edo=0;
                     iArr[ren][col+1][cap].color=ROSA;                                 
                     iArr[ren][col+1][cap].edo=1;
                  }
               }
}

void mueve_rUp( CUBO iArr[N][N][N], int renCub, int *perder)
{
   int ren, col, cap;
   
   for( cap=0; cap<N; cap++)
      for( ren=0; ren<N; ren++)
         for( col=0; col<N; col++)
            if( iArr[ren][col][cap].color==ROSA)         
               if( renCub<ren)                           
               {
                  if(iArr[ren-1][col][cap].color==PLAYER)      
                     *perder=1;                             
                  if(iArr[ren-1][col][cap].color==PLAYER || iArr[ren-1][col][cap].color==NARANJA)  
                  {
                     iArr[ren][col][cap].color=NARANJA;                          
                     iArr[ren][col][cap].edo=0;
                     iArr[ren-1][col][cap].color=ROSA;         
                     iArr[ren-1][col][cap].edo=1;
                  }
               }
}

void mueve_rDown( CUBO iArr[N][N][N], int renCub, int *perder)
{
   int ren, col, cap; 
   
   for( cap=N-1; cap>=0; cap--)
      for( ren=N-1; ren>=0; ren--)
         for( col=N-1; col>=0; col--)
            if( iArr[ren][col][cap].color==ROSA)          
               if( renCub>ren)                            
               {
                  if(iArr[ren+1][col][cap].color==PLAYER)   
                     *perder=1;                            
                  if(iArr[ren+1][col][cap].color==PLAYER || iArr[ren+1][col][cap].color==NARANJA)
                  {
                     iArr[ren][col][cap].color=NARANJA;                
                     iArr[ren][col][cap].edo=0;
                     iArr[ren+1][col][cap].color=ROSA;         
                     iArr[ren+1][col][cap].edo=1;
                  }
               }
}

void mueve_rAtras( CUBO iArr[N][N][N], int capCub, int *perder)
{
   int ren, col, cap;
   
   for( cap=0; cap<N; cap++)
      for( ren=0; ren<N; ren++)
         for( col=0; col<N; col++)
            if( iArr[ren][col][cap].color==ROSA) 
               if( capCub<cap)                             
               {
                  if(iArr[ren][col][cap-1].color==PLAYER)   
                     *perder=1;                            
                  if(iArr[ren][col][cap-1].color==PLAYER || iArr[ren][col][cap-1].color==NARANJA) 
                  {
                     iArr[ren][col][cap].color=NARANJA;                                     
                     iArr[ren][col][cap].edo=0;
                     iArr[ren][col][cap-1].color=ROSA;                                   
                     iArr[ren][col][cap-1].edo=1;
                  }
               }
}

void mueve_rAdelante( CUBO iArr[N][N][N], int capCub, int *perder)
{
   int ren, col, cap;
   
   for( cap=N-1; cap>=0; cap--)
      for( ren=N-1; ren>=0; ren--)
         for( col=N-1; col>=0; col--)
            if( iArr[ren][col][cap].color==ROSA)     
               if( capCub>cap)                          
               {
                  if(iArr[ren][col][cap+1].color==PLAYER)  
                     *perder=1;                            
                  if(iArr[ren][col][cap+1].color==PLAYER || iArr[ren][col][cap+1].color==NARANJA)
                  {
                     iArr[ren][col][cap].color=NARANJA;                       
                     iArr[ren][col][cap].edo=0;
                     iArr[ren][col][cap+1].color=ROSA;      
                     iArr[ren][col][cap+1].edo=1;
                  }
               }
}
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*Giros del contenedor.*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
void gira_Der( CUBO iArr[N][N][N])
{
   int ren, col, cap;
   CUBO iArr2[N][N][N];

   for( cap=0; cap<N; cap++)
      for( ren=0; ren<N; ren++)
         for( col=0; col<N; col++)                                  
         {
            iArr2[ren][col][cap].edo=iArr[ren][N-(cap+1)][col].edo; 
            iArr2[ren][col][cap].color=iArr[ren][N-(cap+1)][col].color;
         }
   for( cap=0; cap<N; cap++)
      for( ren=0; ren<N; ren++)
         for( col=0; col<N; col++)
         {
            iArr[ren][col][cap].edo=iArr2[ren][col][cap].edo;        
            iArr[ren][col][cap].color=iArr2[ren][col][cap].color;
         }
}

void gira_Izq( CUBO iArr[N][N][N])
{
   int ren, col, cap;
   CUBO iArr2[N][N][N];

   for( cap=0; cap<N; cap++)
      for( ren=0; ren<N; ren++)
         for( col=0; col<N; col++)                
         {
            iArr2[ren][N-(cap+1)][col].edo=iArr[ren][col][cap].edo;   
            iArr2[ren][N-(cap+1)][col].color=iArr[ren][col][cap].color;
         }
   for( cap=0; cap<N; cap++)
      for( ren=0; ren<N; ren++)
         for( col=0; col<N; col++)
         {
            iArr[ren][col][cap].edo=iArr2[ren][col][cap].edo; 
            iArr[ren][col][cap].color=iArr2[ren][col][cap].color;
         }
}

void gira_up( CUBO iArr[N][N][N])
{
   int ren, col, cap;
   CUBO iArr2[N][N][N];

   for( cap=0; cap<N; cap++)
      for( ren=0; ren<N; ren++)
         for( col=0; col<N; col++)        
         {
            iArr2[N-(cap+1)][col][ren].edo=iArr[ren][col][cap].edo;   
            iArr2[N-(cap+1)][col][ren].color=iArr[ren][col][cap].color;
         }
   for( cap=0; cap<N; cap++)
      for( ren=0; ren<N; ren++)
         for( col=0; col<N; col++)
         {
            iArr[ren][col][cap].edo=iArr2[ren][col][cap].edo; 
            iArr[ren][col][cap].color=iArr2[ren][col][cap].color;
         }
}

void gira_down( CUBO iArr[N][N][N])
{
   int ren, col, cap;
   CUBO iArr2[N][N][N];

   for( cap=0; cap<N; cap++)
      for( ren=0; ren<N; ren++)
         for( col=0; col<N; col++)                    
         {
            iArr2[ren][col][cap].edo=iArr[N-(cap+1)][col][ren].edo;  
            iArr2[ren][col][cap].color=iArr[N-(cap+1)][col][ren].color;
         }
   for( cap=0; cap<N; cap++)
      for( ren=0; ren<N; ren++)
         for( col=0; col<N; col++)
         {
            iArr[ren][col][cap].edo=iArr2[ren][col][cap].edo;   
            iArr[ren][col][cap].color=iArr2[ren][col][cap].color;
         }
}

void cubosAbajo( CUBO iArr[N][N][N], int *perder)
{
   int ren, col, cap;
   
   for( cap=0; cap<N; cap++)
      for( ren=0; ren<N-1; ren++)
         for( col=0; col<N; col++)				
         {
            if( iArr[ren][col][cap].color==ROSA)		
            {
               if(iArr[ren+1][col][cap].color==NARANJA && *perder==0)
               {
                  iArr[ren][col][cap].color=NARANJA;
                  iArr[ren][col][cap].edo=0;
                  iArr[ren+1][col][cap].color=ROSA;
                  iArr[ren+1][col][cap].edo=1;
               }
               if(iArr[ren+1][col][cap].color==PLAYER)		
               {
                  iArr[ren][col][cap].color=NARANJA;
                  iArr[ren][col][cap].edo=0;
                  iArr[ren+1][col][cap].color=ROSA;
                  iArr[ren+1][col][cap].edo=1;
                  *perder=1;
               }
            }
            else
               if( iArr[ren][col][cap].color==PLAYER)	
               {
                  if(iArr[ren+1][col][cap].color==NARANJA && *perder==0) 
                  {
                     iArr[ren][col][cap].color=NARANJA;
                     iArr[ren][col][cap].edo=0;
                     iArr[ren+1][col][cap].color=PLAYER;
                     iArr[ren+1][col][cap].edo=1;
                  }
                  if(iArr[ren+1][col][cap].color==ROSA)	
                  {
                     iArr[ren][col][cap].color=NARANJA;
                     iArr[ren][col][cap].edo=0;
                     iArr[ren+1][col][cap].color=PLAYER;
                     iArr[ren+1][col][cap].edo=1;
                     *perder=1;
                  }
               }
         }
}

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*Datos del jugador.*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

void records( int puntos)
{
   RECORD a[REC];
   FILE *f;
   int i, y=190;
   char cadena[50];

   cleardevice();                                
   setbkcolor( PANT);                           
   setfillstyle( 1, PANT);                       
   bar( 0, 0, getmaxx(), getmaxy());              
   nombre_jugador( cadena);                     
   cleardevice();                           
   bar( 0, 0, getmaxx(), getmaxy());            
   f=fopen( "Records.dat", "ab");              
   if( f!=NULL){                               
      fseek( f, 0, SEEK_END);               
      i=ftell( f)/sizeof( RECORD);                
      if( i<N)
         for( ; i<REC; i++){                  
            a[i].nombre[0]='\0';
            a[i].puntos=-1;
         }
         fwrite( &a, sizeof( RECORD), REC, f);    
   }
   fclose( f);                                  
   f=fopen( "Records.dat", "rb+");             
   fread( &a, sizeof( RECORD), REC, f);           
   strcpy( a[REC-1].nombre, cadena);             
   a[REC-1].puntos=puntos;                      
   burbuja( a);                                   
   setcolor( SHADOW_1);                     
   outtextxy( (getmaxx()-textwidth( "Mejores puntuaciones:"))/2, y-80, "Mejores puntuaciones:"); 
   setcolor( BLACK);                            
   for( i=0; i<REC-1; i++)
      if( a[i].puntos>=0)                     
      {
         y+=textheight( "H")+10;             
         cadena[0]='\0';                          
         sprintf( cadena, "%d", a[i].puntos);   
         outtextxy( ((getmaxx()-getmaxx()%3)/3)*2-textwidth( a[i].nombre)-20, y, a[i].nombre); 
         outtextxy( ((getmaxx()-getmaxx()%3)/3)*2+20, y, cadena);                         
      }
   rewind( f);                             
   fwrite( &a, sizeof( RECORD), REC, f);        
   getch();                              
   fclose( f);                          
}

void nombre_jugador(char cadena[])
{
   int i, xText, yText;
   
   xText=( getmaxx()-getmaxx()%4)/4-textwidth( cadena)/2;    
   yText=( getmaxy()-getmaxy()%3)*2/3-textheight( cadena)/2;
   setcolor( SHADOW_1);                                      
   strcpy( cadena, "NOMBRE: ");                
   settextstyle( 4, 0, 4);                        
   outtextxy( xText, yText, cadena);                      
   lee_texto( xText+textwidth( cadena), yText, cadena);  
}

void lee_texto( int xText, int yText, char cadena[])
{
   char tecla;
   int contador=0;
    
   cadena[0]='\0';                                     
   do{
      do{                                                  
         setcolor( LIGHTGRAY);                       
         outtextxy(xText+textwidth(cadena), yText, "_");
         retraso(50);                                  
         setcolor( PANT);
         outtextxy(xText+textwidth(cadena), yText, "_");   
         retraso(50);
      }while( !kbhit());
      setcolor(BLACK);
      tecla=getch();
      if(tecla==0)                                  
         tecla=getch();
      if(tecla==8 && contador>0)                         
      {
         setcolor( PANT);                           
         outtextxy(xText, yText, cadena);             
         cadena[--contador]='\0';                
         setcolor(BLACK);                          
         outtextxy(xText, yText, cadena);         
      }
      else
      {
         if(tecla!=13)                        
         {
            cadena[contador++]=tecla;        
            cadena[contador]='\0';
            outtextxy(xText, yText, cadena);
         }
      }
   }while(tecla!=13);
}

void retraso( int milisec)
{
   clock_t i, j;

   i=clock();   
   do{
      j=clock();        
   }while((j-i)<milisec);  
}

void burbuja( RECORD a[REC])
{
    RECORD j;                        
    int rec, i;

    for( rec=1; rec<REC; rec++)          
        for( i=0; i<REC-rec; i++)            
            if( a[i].puntos<a[i+1].puntos ) 
            {        
                j=a[i];             
	            a[i]=a[i+1];
	            a[i+1]=j;
            }
}
